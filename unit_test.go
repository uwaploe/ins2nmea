package main

import (
	"testing"

	"bitbucket.org/uwaploe/go-ins"
)

const REF_TIME int64 = 1584395500

func TestGll(t *testing.T) {
	table := []struct {
		in  ins.DataRecord
		out string
	}{
		{
			in: ins.DataRecord{Tsec: REF_TIME, Lat: 47.5,
				Lon: -122.3, GnssInfo: [2]uint8{0, 0x1c}},
			out: "$GPGLL,4730.0000,N,12218.0000,W,215140.00,A,A*70",
		},
		{
			in: ins.DataRecord{Tsec: REF_TIME, Lat: -47.5,
				Lon: -122.3, GnssInfo: [2]uint8{0, 0x1c}},
			out: "$GPGLL,4730.0000,S,12218.0000,W,215140.00,A,A*6D",
		},
		{
			in: ins.DataRecord{Tsec: REF_TIME, Lat: 47.5,
				Lon: 122.3, GnssInfo: [2]uint8{0, 0x1c}},
			out: "$GPGLL,4730.0000,N,12218.0000,E,215140.00,A,A*62",
		},
		{
			in: ins.DataRecord{Tsec: REF_TIME, Lat: 47.5,
				Lon: -122.3, GnssInfo: [2]uint8{0, 0}},
			out: "$GPGLL,4730.0000,N,12218.0000,W,215140.00,V,E*63",
		},
	}

	for _, e := range table {
		s := createGLL(e.in)
		out := s.String()
		if out != e.out {
			t.Errorf("Bad output; expected %q, got %q", e.out, out)
		}
	}
}

func TestHdt(t *testing.T) {
	table := []struct {
		in     ins.DataRecord
		out    string
		offset float64
	}{
		{
			in:  ins.DataRecord{Heading: 42.421},
			out: "$GPHDT,42.42,T*35",
		},
		{
			in:     ins.DataRecord{Heading: 42.421},
			out:    "$GPHDT,43.42,T*34",
			offset: 1,
		},
		{
			in:     ins.DataRecord{Heading: 42.421},
			out:    "$GPHDT,41.22,T*30",
			offset: -1.2,
		},
	}

	for _, e := range table {
		s := createHDT(e.in, e.offset)
		out := s.String()
		if out != e.out {
			t.Errorf("Bad output; expected %q, got %q", e.out, out)
		}
	}
}

func TestGga(t *testing.T) {
	table := []struct {
		in  ins.DataRecord
		out string
	}{
		{
			in: ins.DataRecord{Tsec: REF_TIME, Lat: 47.5,
				Lon: -122.3, Alt: 42.5, GnssInfo: [2]uint8{0, 0x1c},
				SvInfo: [8]uint8{12, 8}},
			out: "$GPGGA,215140.00,4730.0000,N,12218.0000,W,1,08,,42.5,M,,,,*33",
		},
		{
			in: ins.DataRecord{Tsec: REF_TIME, Lat: 47.5,
				Lon: -122.3, Alt: 42.5, GnssInfo: [2]uint8{0, 0},
				SvInfo: [8]uint8{12, 8}},
			out: "$GPGGA,215140.00,4730.0000,N,12218.0000,W,0,08,,42.5,M,,,,*32",
		},
	}

	for _, e := range table {
		s := createGGA(e.in)
		out := s.String()
		if out != e.out {
			t.Errorf("Bad output; expected %q, got %q", e.out, out)
		}
	}
}
