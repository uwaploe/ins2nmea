package main

import (
	"fmt"

	"bitbucket.org/mfkenney/go-nmea"
	"bitbucket.org/uwaploe/go-ins"
)

// Convert an angle from degrees to degrees and minutes
func todm(d float64) (int, float64) {
	deg := int(d)
	min := (d - float64(deg)) * 60
	return deg, min
}

func createGGA(rec ins.DataRecord) nmea.Sentence {
	s := nmea.Sentence{}
	s.Id = "GPGGA"
	s.Fields = make([]string, 14)

	s.Fields[0] = rec.Timestamp().Format(TIME_FMT)

	d, m := todm(rec.Lat)
	if d < 0 || m < 0 {
		s.Fields[1] = fmt.Sprintf("%02d%07.4f", -d, -m)
		s.Fields[2] = "S"
	} else {
		s.Fields[1] = fmt.Sprintf("%02d%07.4f", d, m)
		s.Fields[2] = "N"
	}

	d, m = todm(rec.Lon)
	if d < 0 || m < 0 {
		s.Fields[3] = fmt.Sprintf("%03d%07.4f", -d, -m)
		s.Fields[4] = "W"
	} else {
		s.Fields[3] = fmt.Sprintf("%03d%07.4f", d, m)
		s.Fields[4] = "E"
	}

	if (rec.GnssInfo[1] & ValidGnss) == ValidGnss {
		s.Fields[5] = "1"
	} else {
		s.Fields[5] = "0"
	}

	s.Fields[6] = fmt.Sprintf("%02d", int(rec.SvInfo[1]))
	s.Fields[8] = fmt.Sprintf("%.1f", rec.Alt)
	s.Fields[9] = "M"

	return s
}

func createGLL(rec ins.DataRecord) nmea.Sentence {
	s := nmea.Sentence{}
	s.Id = "GPGLL"
	s.Fields = make([]string, 7)

	d, m := todm(rec.Lat)
	if d < 0 || m < 0 {
		s.Fields[0] = fmt.Sprintf("%02d%07.4f", -d, -m)
		s.Fields[1] = "S"
	} else {
		s.Fields[0] = fmt.Sprintf("%02d%07.4f", d, m)
		s.Fields[1] = "N"
	}

	d, m = todm(rec.Lon)
	if d < 0 || m < 0 {
		s.Fields[2] = fmt.Sprintf("%03d%07.4f", -d, -m)
		s.Fields[3] = "W"
	} else {
		s.Fields[2] = fmt.Sprintf("%03d%07.4f", d, m)
		s.Fields[3] = "E"
	}

	s.Fields[4] = rec.Timestamp().Format(TIME_FMT)
	if (rec.GnssInfo[1] & ValidGnss) == ValidGnss {
		s.Fields[5] = "A"
		s.Fields[6] = "A"
	} else {
		s.Fields[5] = "V"
		s.Fields[6] = "E"
	}

	return s
}

func createHDT(rec ins.DataRecord, offset float64) nmea.Sentence {
	s := nmea.Sentence{}
	s.Id = "GPHDT"
	s.Fields = make([]string, 2)

	s.Fields[0] = fmt.Sprintf("%.2f", rec.Heading+float32(offset))
	s.Fields[1] = "T"

	return s
}
