package main

import (
	"log"
	"time"

	"bitbucket.org/uwaploe/go-ins"
	"bitbucket.org/uwaploe/gss"
	"github.com/lcm-proj/lcm/lcm-go/lcm"
)

// Worker to read ins.DataRecord values from a channel and use them to create
// and publish a position update command for the GS4.
func publisher(l lcm.LCM, ch <-chan ins.DataRecord) {
	chpub, cherr := l.Publisher("OPENINS_NAV_CMD")

	go func() {
		for err := range cherr {
			log.Printf("LCM publish error: %v", err)
		}
	}()

	defer close(chpub)
	msg := &gss.GssPcommsT{
		SenderId: "DATA_HUB",
	}

	for rec := range ch {
		// Create pcomm_t message, encode and publish
		ns := time.Now().UnixNano()
		msg.TimeUnixSec = float64(ns) / 1e9
		msg.CountPublish++
		msg.Analogs = []gss.GssAnalogT{
			gss.GssAnalogT{Name: "U_INIT_LAT", Value: rec.Lat},
			gss.GssAnalogT{Name: "U_INIT_LON", Value: rec.Lon},
			gss.GssAnalogT{Name: "U_INITIALIZE_HEADING", Value: float64(rec.Heading)},
		}
		msg.Digitals = []gss.GssDigitalT{
			gss.GssDigitalT{Name: "U_INIT_POS", Value: true},
			gss.GssDigitalT{Name: "U_USE_INITIALIZE_HEADING", Value: true},
		}
		b, err := msg.Encode()
		if err != nil {
			log.Printf("LCM encode error: %v", err)
			continue
		}
		chpub <- b
	}
}
