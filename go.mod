module bitbucket.org/uwaploe/ins2nmea

go 1.13

require (
	bitbucket.org/mfkenney/go-nmea v1.2.1
	bitbucket.org/uwaploe/go-ins v1.5.0
	bitbucket.org/uwaploe/gss v1.5.0
	github.com/lcm-proj/lcm v1.4.0
	github.com/nats-io/stan.go v0.6.0
	github.com/vmihailenco/msgpack v4.0.4+incompatible
)
