// Ins2nmea generates NMEA sentences from the Interial Labs GPS/INS unit
// to pass to the Greensea GS4 INS.
package main

import (
	"flag"
	"fmt"
	"log"
	"net"
	"os"
	"os/signal"
	"runtime"
	"syscall"
	"time"

	"bitbucket.org/mfkenney/go-nmea"
	"bitbucket.org/uwaploe/go-ins"
	"github.com/lcm-proj/lcm/lcm-go/lcm"
	"github.com/nats-io/stan.go"
	"github.com/vmihailenco/msgpack"
)

const Usage = `Usage: ins2nmea [options] host:port

Subscribe to the INS data and generate NMEA sentences to pass to the GS4
at HOST:PORT.
`

var Version = "dev"
var BuildDate = "unknown"

var (
	showVers = flag.Bool("version", false,
		"Show program version information and exit")
	debugMode = flag.Bool("debug", false,
		"Log debugging output")
	natsURL        string = "nats://localhost:4222"
	clusterID      string = "must-cluster"
	insSubject     string = "ins.data"
	protocol       string = "udp"
	updateInterval time.Duration
	useGGA         bool
	hdgOffset      float64
)

// Format of time field in GLL sentence
const TIME_FMT = "150405.00"

// Value of GNSSInfo[1] which indicates a valid fix
const ValidGnss uint8 = 0x1c

func parseCmdLine() []string {
	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, Usage)
		flag.PrintDefaults()
	}

	flag.StringVar(&natsURL, "nats-url", lookupEnvOrString("NATS_URL", natsURL),
		"URL for NATS Streaming Server")
	flag.StringVar(&clusterID, "cid", lookupEnvOrString("NATS_CLUSTER_ID", clusterID),
		"NATS cluster ID")
	flag.StringVar(&insSubject, "sub", lookupEnvOrString("INS_SUBJECT", insSubject),
		"Subject name for INS data")
	flag.StringVar(&protocol, "proto", lookupEnvOrString("GS4_PROTOCOL", protocol),
		"Network protocol for GS4")
	flag.BoolVar(&useGGA, "gga", false, "Send GGA sentences rather than GLL")
	flag.Float64Var(&hdgOffset, "offset", lookupEnvOrFloat("INS_HDG_OFFSET", hdgOffset),
		"Heading offset value")
	flag.DurationVar(&updateInterval, "update",
		lookupEnvOrDuration("GS4_POS_UPDATE", updateInterval),
		"Time between GS4 position updates")
	flag.Parse()

	if *showVers {
		fmt.Fprintf(os.Stderr, "%s %s\n", os.Args[0], Version)
		fmt.Fprintf(os.Stderr, "  Built with: %s\n", runtime.Version())
		os.Exit(0)
	}

	return flag.Args()
}

func main() {
	args := parseCmdLine()
	if len(args) == 0 {
		flag.Usage()
		os.Exit(1)
	}

	gs4Addr := args[0]

	var (
		lc     lcm.LCM
		err    error
		status int
		ch     chan ins.DataRecord
	)
	defer func() { os.Exit(status) }()

	if updateInterval != 0 {
		lc, err = lcm.New()
		if err != nil {
			log.Printf("Cannot connect to LCM: %v", err)
			status = 1
			return
		}
		ch = make(chan ins.DataRecord, 1)
		go publisher(lc, ch)
		defer close(ch)
	}
	defer lc.Destroy()

	sc, err := stan.Connect(clusterID, "gs4-nmea", stan.NatsURL(natsURL))
	if err != nil {
		log.Printf("Cannot connect: %v", err)
		status = 1
		return
	}
	defer sc.Close()

	conn, err := net.Dial(protocol, gs4Addr)
	if err != nil {
		log.Print("Cannot access GS4")
		status = 1
		return
	}

	var t_update time.Time
	insCb := func(m *stan.Msg) {
		var rec ins.DataRecord
		err := msgpack.Unmarshal(m.Data, &rec)
		if err != nil {
			log.Printf("Msgpack decode error: %v", err)
			return
		}

		if (rec.GnssInfo[1] & ValidGnss) == ValidGnss {
			var s nmea.Sentence
			if useGGA {
				s = createGGA(rec)
			} else {
				s = createGLL(rec)
			}
			fmt.Fprintf(conn, "%s\r\n", s)
			if *debugMode {
				log.Println(s)
			}
			s = createHDT(rec, hdgOffset)
			fmt.Fprintf(conn, "%s\r\n", s)
			if *debugMode {
				log.Println(s)
			}
			t0 := time.Now()
			if updateInterval != 0 && t0.Sub(t_update) >= updateInterval {
				select {
				case ch <- rec:
					t_update = t0
					log.Println("Update command sent")
				}
			}
		}
	}

	sub, err := sc.Subscribe(insSubject, insCb, stan.StartWithLastReceived())
	if err != nil {
		log.Fatalf("Cannot subscribe to INS data: %v", err)
	}
	defer sub.Unsubscribe()

	// Initialize signal handler
	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM, syscall.SIGHUP)
	defer signal.Stop(sigs)

	log.Printf("GS4 NMEA service starting %s", Version)

	// Exit on a signal
	<-sigs
}
